package com.stanislav.ait.ui.main.views;

import com.stanislav.ait.ui.base.views.BaseView;

public interface MainView extends BaseView {
    //Метод который будет вызывать когда данные от Api загрузятся
    void onLoadQuestions();
}
