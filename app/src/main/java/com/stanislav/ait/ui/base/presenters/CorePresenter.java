package com.stanislav.ait.ui.base.presenters;

import com.stanislav.ait.BaseApplication;
import com.stanislav.ait.utils.api.ApiService;

import javax.inject.Inject;


public class CorePresenter {

    //для запроса зависимости
    /*
    здесь мы хотим получить от дагера зависимость
    берёт из модулей и засовывает в Inject
    * */
    //иньекция в поле
    //@Inject – базовая аннотация, с помощью которой “запрашивается зависимость”
    @Inject protected ApiService mApiService;

    //конструктор CorePresenter
    public CorePresenter() {
        //создание компонента
        BaseApplication.getAppComponent().inject(this);
    }
}
