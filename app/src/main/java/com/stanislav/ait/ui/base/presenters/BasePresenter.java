package com.stanislav.ait.ui.base.presenters;

import com.stanislav.ait.ui.base.views.BaseView;


public class BasePresenter<T extends BaseView> extends CorePresenter {

    protected T mView;

    public BasePresenter(T view) {
        super();
        this.mView = view;
    }
}
