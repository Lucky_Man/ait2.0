package com.stanislav.ait.ui.main.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.stanislav.ait.R;
import com.squareup.picasso.Picasso;


/**
 * Created by Las Vegas on 01.12.2016.
 */

public class Image_fragment extends Fragment {

    private String PictureUrl;
    public ImageView PictuteTest;

    //Метод для установки аргументов фрагменту , вызовем перед тем как запустить фрагмент
    public static Image_fragment newInstance (String picture)
    {
        Image_fragment image_fragment  = new Image_fragment();
        Bundle args = new Bundle();
        args.putString("picture",picture);
        image_fragment.setArguments(args);
        return  image_fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        PictureUrl = getArguments().getString("picture");

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.item_test, container, false);

        PictuteTest = (ImageView) v.findViewById(R.id.picture_test);
        Picasso.with(getContext())
                //здесь грузим изображение
                .load(PictureUrl)
                .into(PictuteTest);
        return v;
    }
}
