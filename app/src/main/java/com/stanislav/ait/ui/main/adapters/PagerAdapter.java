package com.stanislav.ait.ui.main.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.stanislav.ait.ui.main.fragments.Image_fragment;
import com.stanislav.ait.ui.main.presenters.MainPresenter;

/**
 * Created by Las Vegas on 09.12.2016.
 */

public class PagerAdapter extends FragmentPagerAdapter {

    String picture;
    MainPresenter mPresenter;
    private int mCount;

    public PagerAdapter(FragmentManager fm , MainPresenter mainPresenter , int count) {
        super(fm);
        mPresenter = mainPresenter;
        mCount = count;
    }

    @Override
    public Fragment getItem(int position) {

        Image_fragment image_fragment = new Image_fragment();

        if(mPresenter.getQuestions().size() !=0) {
            picture = mPresenter.getQuestions().get(position).getPicture();
        }

        return image_fragment.newInstance(picture);
    }
    @Override
    //возвращает текущее количество элементов в списке
    public int getCount() {
        return mCount;
    }
}

