package com.stanislav.ait.ui.main.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.stanislav.ait.R;
import com.stanislav.ait.ui.base.activities.BaseActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.text.TextUtils.isEmpty;

public class AuthenticatieActivity extends BaseActivity implements View.OnClickListener{

    private EditText mETemail;
    private EditText mETpassword;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    // [END declare_auth]

    // [START declare_auth_listener]
    private FirebaseAuth.AuthStateListener mAuthListener;
    // [END declare_auth_listener]

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authenticatie);

        mETemail = (EditText) findViewById(R.id.et_email);
        mETpassword = (EditText) findViewById(R.id.et_password);

        mDatabase = FirebaseDatabase.getInstance().getReference();// получение ссылки на базу данных

        findViewById(R.id.btn_sigin).setOnClickListener(this);

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]
        // [START auth_state_listener]
        mAuthListener = new FirebaseAuth.AuthStateListener() {

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                //Получаем текущего пользователя
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("OK", "onAuthStateChanged:signed_in:" + user.getUid());
                    getData (user.getUid());
                    //Создаём интент для активности
                } else {
                    // User is signed out
                }
            }
        };

        FirebaseUser user = mAuth.getCurrentUser();
        if(user != null) {
            String userUID = user.getUid();
            getData (userUID);
        }

        // [END auth_state_listener]
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_sigin) {
            //Если нажата кнопка - попытка входа
            signIn(mETemail.getText().toString(), mETpassword.getText().toString());
            FirebaseUser user = mAuth.getCurrentUser();
            if(user != null) {
                String userUID = user.getUid();
                getData (userUID);
            }
        }
    }
    // [START on_start_add_listener]
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    // [END on_start_add_listener]

    // [START on_stop_remove_listener]
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    //Вход в аккаунт
    private void signIn(String email, String password) {
        //Валидация форм
        if (!validateForm()) {
            return;
        }
        //Показываем колесо прокрутки
        showProgressDialog();
        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                //Слушатель для выполненой задачи
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    //При завершение задачи выполнится это метод
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            //Log.w(TAG, "signInWithEmail:failed", task.getException());
                            Toast.makeText(AuthenticatieActivity.this, R.string.auth_failed,
                                    Toast.LENGTH_SHORT).show();
                        }
                        if (task.isSuccessful()) {
                            Toast.makeText(AuthenticatieActivity.this,"Вход выполнен",
                                    Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]
    }

    private boolean validateForm() {
        boolean valid = true;

        //Получаем почту из поле ввожда
        String email = mETemail.getText().toString();
        if (isEmpty(email)) {//если значение пустое
            mETemail.setError("Required.");
            valid = false;
        } else {
            mETemail.setError(null);
        }
        String password = mETpassword.getText().toString();
        if (isEmpty(password)) {
            mETpassword.setError("Required.");
            valid = false;
        } else {
            mETpassword.setError(null);
        }
        //Возвращаем значение валидации
        return valid;
    }

    //Получаем данные по уникальному идентификатору пользователя
    private void getData (String userUID){
        //child - спускает нас ниже по иерархия базы данных
        //Добаляем слушателя для определённо строки в базе данных
        mDatabase.child("users").child(userUID).child("link").addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    //Вызовется когда данные изменятся или инициализируются
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        String link = dataSnapshot.getValue(String.class);
                        if (link != null)
                        {
                            Intent intent = new Intent(AuthenticatieActivity.this, TestPagerActivity.class);
                            intent.putExtra("link",link);
                            startActivity(intent);
                        }
                        // [END_EXCLUDE]
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
    }
}
