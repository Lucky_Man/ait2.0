package com.stanislav.ait.ui.main.presenters;


import android.util.Log;

import com.stanislav.ait.R;
import com.stanislav.ait.ui.base.presenters.BasePresenter;

import com.stanislav.ait.ui.main.views.MainView;

import com.stanislav.ait.utils.api.models.response.BaseResponse;
import com.stanislav.ait.utils.api.models.response.CategoryResponse;
import com.stanislav.ait.utils.api.models.response.InterfaceResponse;
import com.stanislav.ait.utils.api.models.response.QuestionsResponse;
import com.stanislav.ait.utils.api.models.response.ResultReposne;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/*
для связи данных со вьюхами
вызывается в recycler view для получения данных
* */
public class MainPresenter extends BasePresenter<MainView> {

    private static final String TAG = MainPresenter.class.getSimpleName();
    private String link;
    //наш список категорий
    private List<QuestionsResponse> mCategories = new ArrayList<>();
    private InterfaceResponse mElements = new InterfaceResponse();
    private List<CategoryResponse> mCategory = new ArrayList<>();

    public MainPresenter(MainView view) {
        super(view);
    }

    public  void setLink (String Link){
        link = Link;
    }


    //загрузка категори
    //mApiService находится в corePresnter как @inject
    //вызываем в главном активити для загрузки категорий
    public void loadCategories() {
        Call<BaseResponse<ResultReposne>> request = mApiService.getApi().getTestWithServer(link);//Делаем асинхронный запрос
        request.enqueue(mGetCategoriesCallback);//Передаём обьект Callback
    }

    public List<QuestionsResponse> getQuestions() {
        return mCategories;
    }

    public InterfaceResponse getElements(){return mElements;}

    public List<CategoryResponse> getCategory() {
        return mCategory;
    }

    private Callback<BaseResponse<ResultReposne>> mGetCategoriesCallback = new Callback<BaseResponse<ResultReposne>>() {

        /**после того как запрос выполнен в главном
         * потоке вызывает метод onResponse или onFailure, в зависимости от результата.
        */
        @Override
        public void onResponse(Call<BaseResponse<ResultReposne>> call, Response<BaseResponse<ResultReposne>> response) {
            if (response.code() == 200) {
                BaseResponse<ResultReposne> body = response.body();//Для извлечения ответа от сервера
                //Извлекаем из ответа обьект ErrorResponse error из него извлекаем поле success если оно true
                //извлекаем массив question
                if (body.getError().getSuccess()) {
                    //getResult() есть в BaseResponse как метод который возвращает в данном случае обьект типа QuestionsResponse
                    mCategories = body.getResult().getQuestions();
                    mElements = body.getResult().getElements();
                    mCategory = body.getResult().getCategory();
                } else {
                    mView.onShowError(R.string.error_message_wrong_api_request);
                }
            } else {
                mView.onShowError(R.string.error_message_wrong_api_request);
            }

            mView.onLoadQuestions();//В текущем View запустим наш метод
        }

        @Override
        public void onFailure(Call<BaseResponse<ResultReposne>> call, Throwable t) {
            Log.e(TAG, t.getMessage(), t);
            mView.onShowError(R.string.error_message_wrong_api_request);
        }
    };


}
