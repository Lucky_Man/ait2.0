package com.stanislav.ait.ui.base.views;


public interface BaseView {
    void onShowError(int resId);
    void onShowProgress(boolean isShow);
}
