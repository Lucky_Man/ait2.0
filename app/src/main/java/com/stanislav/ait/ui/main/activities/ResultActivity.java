package com.stanislav.ait.ui.main.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.stanislav.ait.R;
import com.stanislav.ait.ui.main.adapters.ResultListAdapter;
import com.stanislav.ait.ui.main.models.CategoryInterest;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    private ArrayList <CategoryInterest> ResultTest = new ArrayList<>();
    private int countQuestion;
    private RecyclerView mRecyclerView;
    private ResultListAdapter mAdapter;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        mAuth = FirebaseAuth.getInstance();
        Intent intent = getIntent();
        countQuestion = intent.getIntExtra("CountCategory",0);

        for (int i = 0 ; i<countQuestion;i++) {
            CategoryInterest categoryInterest = new CategoryInterest();
            categoryInterest.setTitle(intent.getStringExtra("category" + i));
            categoryInterest.setPoints(intent.getIntExtra("point" + i,i));
            categoryInterest.setPicture(intent.getStringExtra("picture1" + i));
            int pos = posInsert(categoryInterest);
            ResultTest.add(pos,categoryInterest);
        }

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mAdapter = new ResultListAdapter(this,ResultTest);
        mRecyclerView = (RecyclerView)findViewById(R.id.test_recycler_view);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mAdapter);

    }

    int posInsert(CategoryInterest categoryInterest) {
        int j;
        if (ResultTest.size() != 0) {
            for (j=0;j<ResultTest.size();j++) {
                if(categoryInterest.getPoints()> ResultTest.get(j).getPoints()) {
                 return j;
                }
            }
            return j;
        }
        else
            return 0;
    }

    void onClickBtnLogOut (View v){
        mAuth.signOut();
        Intent intent = new Intent(ResultActivity.this,AuthenticatieActivity.class);
        startActivity(intent);
    }
}
