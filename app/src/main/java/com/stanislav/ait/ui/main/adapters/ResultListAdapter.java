package com.stanislav.ait.ui.main.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.stanislav.ait.R;
import com.stanislav.ait.ui.main.models.CategoryInterest;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

/**
 * Created by Las Vegas on 14.12.2016.
 */

public class ResultListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<CategoryInterest> ListCategories = new ArrayList<>();

    public ResultListAdapter(Context context, ArrayList<CategoryInterest> answerOnQuestion) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.ListCategories = answerOnQuestion;

    }

    //Создание новой вью
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_test_recycler_view, parent, false);
        return new ResultListAdapter.ViewHolder(view);
    }

    //Заполенния макетов контентом
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder Baseholder, int position) {
        ViewHolder holder = (ViewHolder) Baseholder;

        holder.mCategoryQuestion.setText(ListCategories.get(position).getTitle());
        holder.mPoint.setText("" + ListCategories.get(position).getPoints());

        Picasso.with(mContext)
                //здесь грузим изображение
                .load(ListCategories.get(position).getPicture())
                .into(holder.mIVPicture1);
    }

    //Получение количество элементов
    @Override
    public int getItemCount() {
        if (ListCategories.size()>3)
            return 3;
        return ListCategories.size();
    }

    //Создание макетов
    private class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView mPoint;
        protected TextView mCategoryQuestion;
        protected ImageView mIVPicture1;

        public ViewHolder(View view) {
            super(view);

            mPoint = (TextView) view.findViewById(R.id.tv_point_category);
            mCategoryQuestion = (TextView) view.findViewById(R.id.tv_title_category);
            mIVPicture1 = (ImageView)view.findViewById(R.id.iv_picture_result);
        }
    }
}
