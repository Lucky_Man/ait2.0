package com.stanislav.ait.ui.main.models;

/**
 * Created by Las Vegas on 15.12.2016.
 */

public class CategoryInterest {

    private String Title;
    private int Points;
    private String Picture;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getPoints() {
        return Points;
    }

    public void setPoints(int points) {
        Points = points;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String picture) {
        this.Picture = picture;
    }

}
