package com.stanislav.ait.ui.main.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.stanislav.ait.R;
import com.stanislav.ait.ui.main.adapters.PagerAdapter;
import com.stanislav.ait.ui.main.models.CategoryInterest;
import com.stanislav.ait.ui.main.presenters.MainPresenter;
import com.stanislav.ait.ui.main.views.MainView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;


public class TestPagerActivity extends FragmentActivity implements MainView {

    private MainPresenter mPresenter = new MainPresenter(this);
    private ViewPager mViewPager;
    private TextView mTitleQuestion;
    private int CountQuestion;
    private ImageButton mButtonLike;
    private ImageButton mButtonDisLike;
    private ArrayList<CategoryInterest> mCategoryInterests= new ArrayList <CategoryInterest>();
    private int CurrentAnswer;
    private ProcessingAnswer processingAnswer;
    private int CurrentQuestion;
    private TextToSpeech Say;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);

        Intent intent = getIntent();
        String link = intent.getStringExtra("link");

        mPresenter.setLink(link);
        mPresenter.loadCategories();

        mTitleQuestion = (TextView) findViewById(R.id.title_question);
        mViewPager = (ViewPager) findViewById(R.id.test_view_pager);

        Say=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    Say.setLanguage(Locale.US);
                }
            }
        });

        //Слушатель для viewpager
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                mTitleQuestion.setText(mPresenter.getQuestions().get(position).getTitle());
                Say.stop();
                Say.speak(mPresenter.getQuestions().get(position).getTitle(), TextToSpeech.QUEUE_FLUSH, null);

            }
            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });

        mButtonLike = (ImageButton) findViewById(R.id.button_like);
        mButtonDisLike = (ImageButton) findViewById(R.id.button_dislike);
    }
    public void onClickBtn1(View v)//Like
    {
        forClicklButton(1);
    }
    public void onClickBtn2(View v)//Dislike
    {
        forClicklButton(0);
    }

    public void forClicklButton (int answer)
    {
        CurrentQuestion = mViewPager.getCurrentItem();
        CurrentAnswer = answer;
         //Здесь запустим новую задачу для добавление баллов определённой категории
         processingAnswer = new ProcessingAnswer();
         processingAnswer.execute();
         mViewPager.setCurrentItem(mViewPager.getCurrentItem()+1);
    }

    //Это метод будет вызван как только выполнится GET запрос
    @Override
    public void onLoadQuestions() {
        //получаем фрагмент менеджер для активности
        FragmentManager fragmentManager = getSupportFragmentManager();
        CountQuestion = mPresenter.getQuestions().size();
        mViewPager.setAdapter( new PagerAdapter(fragmentManager,mPresenter,CountQuestion));

            Picasso.with(getBaseContext())
                    //здесь грузим изображение
                    .load(mPresenter.getElements().getPicturelike())
                    .into(mButtonLike);
            Picasso.with(getBaseContext())
                    //здесь грузим изображение
                    .load(mPresenter.getElements().getPicturedislike())
                    .into(mButtonDisLike);

        mTitleQuestion.setText(mPresenter.getQuestions().get(0).getTitle());
        Say.speak(mPresenter.getQuestions().get(0).getTitle(), TextToSpeech.QUEUE_FLUSH, null);

        for(int i=0;i<mPresenter.getCategory().size();i++)
        {
            CategoryInterest categoryInterest = new CategoryInterest();
            categoryInterest.setTitle(mPresenter.getCategory().get(i).getName());
            categoryInterest.setPicture("def");
            mCategoryInterests.add(categoryInterest);
        }
    }

    @Override
    public void onShowError(int resId) {

    }

    @Override
    public void onShowProgress(boolean isShow) {

    }
    public void onPause(){
        if(Say !=null){
            Say.stop();
            //Say.shutdown();
        }
        super.onPause();
    }

    /*
При описании класса-наследника AsyncTask мы в угловых скобках указываем три типа данных:
1) Тип входных данных. Это данные, которые пойдут на вход AsyncTask
2) Тип промежуточных данных. Данные, которые используются для вывода промежуточных результатов
3) Тип возвращаемых данных. То, что вернет AsyncTask после работы.
    * */
    class ProcessingAnswer extends AsyncTask<Void, Void, Void> {
        private String category;
        private String picture;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            category = mPresenter.getQuestions().get(mViewPager.getCurrentItem()).getCategory();
            picture = mPresenter.getQuestions().get(mViewPager.getCurrentItem()).getPicture();
        }

        //В этом методе выполняем основную задачу здесь UI элементы недоступны
        @Override
        protected Void doInBackground(Void... params) {
            for (int i=0;i<mCategoryInterests.size();i++)
            {
                if (mCategoryInterests.get(i).getTitle().equals(category) == true)
                {
                    if(CurrentAnswer == 0)
                    {
                        int point = mCategoryInterests.get(i).getPoints()-1;
                        mCategoryInterests.get(i).setPoints(point);
                    }
                    else
                    {
                        int point = mCategoryInterests.get(i).getPoints()+1;
                        mCategoryInterests.get(i).setPoints(point);
                    }
                    if (mCategoryInterests.get(i).getPicture().equals("def"))
                    {
                        mCategoryInterests.get(i).setPicture(picture);
                    }
                }
            }

            return null;
        }
        //метод который выполнится после завершения выполнения таска
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (mViewPager.getCurrentItem() == CurrentQuestion)
            {
                Intent intent = new Intent(TestPagerActivity.this,ResultActivity.class);
                intent.putExtra("CountCategory",mCategoryInterests.size());

                for (int i = 0 ; i<mCategoryInterests.size();i++) {
                    intent.putExtra("category" + i,mCategoryInterests.get(i).getTitle());
                    intent.putExtra("point" + i,mCategoryInterests.get(i).getPoints());
                    intent.putExtra("picture1" + i,mCategoryInterests.get(i).getPicture());
                }

                startActivity(intent);
            }
        }
    }

}
