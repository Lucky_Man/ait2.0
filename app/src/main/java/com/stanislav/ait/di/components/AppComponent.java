package com.stanislav.ait.di.components;

import android.content.Context;

import com.stanislav.ait.di.modules.ApiServiceModule;
import com.stanislav.ait.di.modules.ContextModule;
import com.stanislav.ait.ui.base.presenters.CorePresenter;

import javax.inject.Singleton;

import dagger.Component;

//Мост между модулями и получателями зависимости
//инжектор

@Singleton
@Component(modules = {ApiServiceModule.class, //в этом модуле создаём новый обьект ApiService
                      ContextModule.class
})
public interface AppComponent {
//набор обьектов которым мы должны удовлетровить зависимость
    Context getContext();

    //функция куда инжектим
    void inject(CorePresenter presenter);
}
