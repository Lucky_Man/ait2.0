package com.stanislav.ait.di.modules;

import com.stanislav.ait.utils.api.ApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


//будет жить на протижение всего обьекта
//@Module – классы, чьи методы “предоставляют зависимости”
@Module
public class ApiServiceModule {

    @Singleton
    @Provides // Говорит нам как мы хотим сконструировать и создать зависимость
    public ApiService provideApiService() {
        return new ApiService();
    }
}
