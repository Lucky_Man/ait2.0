package com.stanislav.ait.utils.api.models.response;

import java.util.List;

public class ResultReposne {

    private List<QuestionsResponse> question;
    private InterfaceResponse elements;
    private List<CategoryResponse> category;

    public List<QuestionsResponse> getQuestions() {
        return question;
    }

    public void setQuestions(List<QuestionsResponse> question) {
        this.question = question;
    }

    public InterfaceResponse getElements() {
        return elements;
    }

    public void setElements(InterfaceResponse elements) {
        this.elements = elements;
    }

    public List<CategoryResponse> getCategory() {
        return category;
    }

    public void setCategory(List<CategoryResponse> category) {
        this.category = category;
    }
}
