package com.stanislav.ait.utils.api.models.response;

/**
 * Created by Las Vegas on 15.11.2016.
 */
public class QuestionsResponse {

    private String title;
    private String picture;
    private String category;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
