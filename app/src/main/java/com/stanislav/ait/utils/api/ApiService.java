package com.stanislav.ait.utils.api;

import com.stanislav.ait.utils.api.models.response.BaseResponse;
import com.stanislav.ait.utils.api.models.response.ResultReposne;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;


public class ApiService {
    private static final String DOMAIN = "https://api.myjson.com"; //Базовый домен
    private IApiService mApi; //Обьект api ретрофита

    //Метод для получения данных
    public IApiService getApi() {
        if (mApi == null) {
            Retrofit retrofit = new Retrofit.Builder() //Создаём обьект ретфрофит
                    .baseUrl(DOMAIN)//Базый url
                    .addConverterFactory(GsonConverterFactory.create())//Какой ковертер будем использоватья для
                    //преобразования из JSON в обьекты Java
                    .build();//Создаём

            //Создаём обькт api ретрофита
            mApi = retrofit.create(IApiService.class);
        }
        return mApi;
    }
    //Создаём интерфейс ретрофита
    public interface IApiService {
        //Аннотация означающая get запрос с манипулируемым url
        @GET("/bins/{id}")
        Call<BaseResponse<ResultReposne>> getTestWithServer(@Path("id") String id_json_file);
    }
}
