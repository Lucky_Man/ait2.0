package com.stanislav.ait.utils.api.models.response;

/**
 * Created by Las Vegas on 10.12.2016.
 */

public class InterfaceResponse {

    private String picturelike;
    private String picturedislike;

    public String getPicturelike() {
        return picturelike;
    }

    public void setPicturelike(String picturelike) {
        this.picturelike = picturelike;
    }

    public String getPicturedislike() {
        return picturedislike;
    }

    public void setPicturedislike(String picturedislike) {
        this.picturedislike = picturedislike;
    }

}
