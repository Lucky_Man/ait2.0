package com.stanislav.ait;

import android.app.Application;

import com.stanislav.ait.di.components.AppComponent;
import com.stanislav.ait.di.components.DaggerAppComponent;
import com.stanislav.ait.di.modules.ApiServiceModule;
import com.stanislav.ait.di.modules.ContextModule;

public class BaseApplication extends Application {

    private static AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder()
                .apiServiceModule(new ApiServiceModule())
                .contextModule(new ContextModule(getApplicationContext()))
                .build();
    }

    public static AppComponent getAppComponent() {
        return mAppComponent;
    }
}
